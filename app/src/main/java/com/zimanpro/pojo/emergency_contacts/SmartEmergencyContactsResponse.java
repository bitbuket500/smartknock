package com.zimanpro.pojo.emergency_contacts;

import java.io.Serializable;
import java.util.ArrayList;

public class SmartEmergencyContactsResponse implements Serializable {

    private String message;

    public ArrayList<Contacts> contacts = new ArrayList<Contacts>();

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contacts> contacts) {
        this.contacts = contacts;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", contacts = "+contacts+", status = "+status+"]";
    }

}
