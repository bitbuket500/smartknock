package com.zimanpro.interfaces;

import android.accounts.NetworkErrorException;

import com.zimanpro.pojo.MultimediaResponse;
import com.zimanpro.pojo.ProfileImageResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ZimanMahindraFinanceApi {

    // Live
//        @POST("/citysmile.in/say_mahindrafinance/backend/api/saveMultimedia")

    // say_mahindrafinance
    // ziman_backend
    @Multipart
//    @POST("/citysmile.in/ziman_backend/backend/api/saveMultimedia")
    @POST("/backend/api/saveMultimedia")
    Call<MultimediaResponse> saveMultiMediaImage(@Part("user_id") RequestBody user_id,
                                                 @Part("panic_id") RequestBody panic_id,
                                                 @Part("user_lat") RequestBody user_lat,
                                                 @Part("user_long") RequestBody user_long,
                                                 @Part("auth_key") RequestBody auth_key,
                                                 @Part("app_security_key") RequestBody app_key,
                                                 @Part("content_type") RequestBody content_type,
                                                 @Part("module_type") RequestBody module_type,
                                                 @Part("notes") RequestBody notes,
                                                 @Part("type") RequestBody type,
                                                 @Part List<MultipartBody.Part> file) throws NetworkErrorException;


    @Multipart
    //@POST("/APIs/update_member_profile_image1.php")  // main url
    @POST("/kk/APIs/update_member_profile_image1.php") // production url
    Call<ProfileImageResponse> saveProfileImage(@Part("mobile_no") RequestBody mobile_no,
                                                @Part List<MultipartBody.Part> file) throws NetworkErrorException;


}
