package com.zimanpro.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanpro.R;
import com.zimanpro.base.ZimanProApplication;
import com.zimanpro.helperClasses.AndroidUtils;
import com.zimanpro.helperClasses.AppConstants;
import com.zimanpro.helperClasses.AppWaitDialog;
import com.zimanpro.helperClasses.InternetConnection;
import com.zimanpro.helperClasses.SessionManager;
import com.zimanpro.interfaces.AppDataUrls;
import com.zimanpro.pojo.registration.RegistrationResponse;
import com.zimanpro.pojo.registration.Result;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    EditText editTextFirstName;
    //    EditText editTextMiddleName;
//    EditText editTextLastName;
    EditText editTextEmailAddress;
    //    EditText editTextLanguage;
    TextView textViewDOB;
    RadioGroup radioGroupGender;
    Button buttonSubmit;
    SessionManager sessionManager;
    private Spinner spBloodGrp;
    private AppWaitDialog mWaitDialog = null;
    private Calendar myCalendar;
    int selectedYear = 2019;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String pattern = "yyyy-dd-MM";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            selectedYear = year;
            String month = "";
            String dayOfMon = "";
            if ((monthOfYear + 1) < 9) {
                month = "0" + (monthOfYear + 1);
            } else {
                month = (monthOfYear + 1) + "";
            }

            if (dayOfMonth < 9) {
                dayOfMon = "0" + dayOfMonth;
            } else {
                dayOfMon = dayOfMonth + "";
            }

            String date = year + "-" + month + "-" + dayOfMon;
            System.out.println("date = " + date);
            textViewDOB.setText(date);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);
        myCalendar = Calendar.getInstance();

        radioGroupGender = (RadioGroup) findViewById(R.id.radioGroupGender);
        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
//        editTextMiddleName = (EditText) findViewById(R.id.editTextMiddleName);
//        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextEmailAddress = (EditText) findViewById(R.id.editTextEmailAddress);
//        editTextLanguage = (EditText) findViewById(R.id.editTextLanguage);
        textViewDOB = (TextView) findViewById(R.id.textViewDOB);
        textViewDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegistrationActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_DARK, dateListener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);


        spBloodGrp = (Spinner) findViewById(R.id.sp_blood_group);
        ArrayAdapter<CharSequence> adapterBlood = ArrayAdapter.createFromResource(
                this, R.array.blood_group, R.layout.spinner_item);
        adapterBlood.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spBloodGrp.setAdapter(adapterBlood);
//        spBloodGrp.setSelection(0);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = editTextFirstName.getText().toString();
//                String middleName = editTextMiddleName.getText().toString();
//                String lastName = editTextLastName.getText().toString();
                String emailAddress = editTextEmailAddress.getText().toString();
//                String language = editTextLanguage.getText().toString();
                String dob = textViewDOB.getText().toString();
                System.out.println("dob = " + dob);
                boolean isValidate = true;
                if (TextUtils.isEmpty(firstName)) {
                    isValidate = false;
                    editTextFirstName.setError("Please enter first name.");
                    editTextFirstName.requestFocus();
                }

//                if (TextUtils.isEmpty(middleName)){
//                    isValidate = false;
//                    editTextMiddleName.setError("Please enter middle name.");
//                    editTextMiddleName.requestFocus();
//                }

//                if (TextUtils.isEmpty(lastName)){
//                    editTextLastName.setError("Please enter last name.");
//                    editTextLastName.requestFocus();
//                }

                if (TextUtils.isEmpty(emailAddress)) {
                    isValidate = false;
                    editTextEmailAddress.setError("Please enter email address.");
                    editTextEmailAddress.requestFocus();
                }

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (!emailAddress.matches(emailPattern)) {
                    isValidate = false;
                    editTextEmailAddress.setError("Please enter valid email address.");
                    editTextEmailAddress.requestFocus();
                }

//                if (TextUtils.isEmpty(language)){
//                    isValidate = false;
//                    editTextLanguage.setError("Please enter your language.");
//                    editTextLanguage.requestFocus();
//                }

                if (TextUtils.isEmpty(dob) && (textViewDOB.getText().toString().contains("Select"))) {
                    isValidate = false;
                    textViewDOB.setError("Please enter date of birth.");
                    textViewDOB.requestFocus();
                }

                if (spBloodGrp.getSelectedItemId() == 0) {
                    isValidate = false;
                    Toast.makeText(RegistrationActivity.this, "Please select blood group.", Toast.LENGTH_SHORT).show();
                    spBloodGrp.requestFocus();
                }

                int currentYear = Calendar.getInstance().get(Calendar.YEAR);

                int age = currentYear - selectedYear;
                if (age < 18) {
                    isValidate = false;
                    Toast.makeText(RegistrationActivity.this, "Age should be grater then 18.", Toast.LENGTH_SHORT).show();
                    textViewDOB.setError("Date of birth should be grater then 18.");
                    textViewDOB.requestFocus();
                }

                int gender = 1;
                if (radioGroupGender.getCheckedRadioButtonId() == R.id.rb_type_male) {
                    gender = 1;
                } else {
                    gender = 2;
                }

                if (isValidate) {
                    showpopup(firstName, emailAddress, dob, age, gender);
                    //signUpApiCall(firstName, emailAddress, dob, age, gender);
                }


            }
        });

//        TextView bldGrpView = (TextView) spBloodGrp.getSelectedView().findViewById(R.id.textView1);
//        sBloodGrp= bldGrpView.getText().toString();
    }

    private void showpopup(final String firstName, final String emailAddress, final String dob, final int age, final int gender) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        //textViewTitle.setText("Your app is uploading users' contact information to api without a prominent disclosure.");
        textViewTitle.setText(R.string.poptext1);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        //TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpApiCall(firstName, emailAddress, dob, age, gender);

                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void signUpApiCall(final String firstName, final String emailAddress, final String dob, final int age, final int gender) {
        AndroidUtils.hideKeyboard(RegistrationActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("SignUp URl = " + AppDataUrls.signup());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.signup(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("SignUp Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            Result result = registrationResponse.result;
                            if (result != null) {
//                                sessionManager.createLoginSession(true);
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
//                                sessionManager.saveSAPCode(result.sapCode);
//                                sessionManager.saveFirstName(result.firstName);
//                                sessionManager.saveMiddleName(result.middleName);
//                                sessionManager.saveLastName(result.lastName);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);

//                                sessionManager.saveBranchName(result.branchName);
                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }
//                                sessionManager.saveDepartment(result.department);
//                                sessionManager.saveDesignation(result.designation);
                                sessionManager.saveDeviceToken(result.deviceToken);
//                                sessionManager.saveActivePlatform(result.activePlatform);
                                sessionManager.saveAuthKey(result.authKey);
//                                sessionManager.saveCreatedAt(result.createdAt);
//                                sessionManager.saveModifiedAt(result.modifiedAt);
//                                sessionManager.saveIsDeleted(result.isDeleted);
                                if (result.emergencyContacts.isEmpty()) {
                                    Intent intent = new Intent(RegistrationActivity.this, AddEmergencyContactActivity.class);
                                    intent.putExtra("From", "Login");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(RegistrationActivity.this, TnCnPolicyActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } else {
                            Toast.makeText(RegistrationActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(RegistrationActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    signUpApiCall(firstName, emailAddress, dob, age, gender);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.FIRST_NAME, firstName);
                params.put(AppConstants.DATE_OF_BIRTH, dob);
                params.put(AppConstants.EMAIL, emailAddress);
                params.put(AppConstants.AGE, age + "");
                params.put(AppConstants.MOBILE_NO, sessionManager.getMobileNo());
                params.put(AppConstants.GENDER, gender + "");
                params.put(AppConstants.BLOOD_GROUP, spBloodGrp.getSelectedItemId() + "");

                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());

                System.out.println("Sign up Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

}
