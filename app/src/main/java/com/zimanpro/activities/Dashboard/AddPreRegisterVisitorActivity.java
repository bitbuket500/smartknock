package com.zimanpro.activities.Dashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.zimanpro.R;
import com.zimanpro.activities.SmartHomeActivity;
import com.zimanpro.helperClasses.AppWaitDialog;
import com.zimanpro.helperClasses.SessionManager;
import com.zimanpro.interfaces.AppDataUrls;

public class AddPreRegisterVisitorActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    private SessionManager sessionManager;
    private WebView webView;
    SharedPreferences mPrefs;
    String MobileNo, FCMId, URL;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pre_register_visitor);

        URL = "";
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        mWaitDialog = new AppWaitDialog(this);
        sessionManager = new SessionManager(this);
        webView = (WebView) findViewById(R.id.webView);

        //webView = findViewById(R.id.webview);

        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        FCMId = mPrefs.getString("SMARTFCMID", "");

        URL = AppDataUrls.WEB_BASE_URL + "validateFCM.php?mobileNumber=" + MobileNo + "&fcmId=" + FCMId + "&pageNo=1";

        startWebView(URL);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void startWebView(String url) {

        System.out.println("Presregister URL: " + url);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        progressDialog = new ProgressDialog(AddPreRegisterVisitorActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(ContestActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(AddPreRegisterVisitorActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }

}
