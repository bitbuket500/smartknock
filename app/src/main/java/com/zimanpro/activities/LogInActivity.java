package com.zimanpro.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.zimanpro.R;
import com.zimanpro.base.ZimanProApplication;
import com.zimanpro.helperClasses.AndroidUtils;
import com.zimanpro.helperClasses.AppConstants;
import com.zimanpro.helperClasses.AppWaitDialog;
import com.zimanpro.helperClasses.InternetConnection;
import com.zimanpro.helperClasses.SessionManager;
import com.zimanpro.interfaces.AppDataUrls;
import com.zimanpro.pojo.login.LogInResponse;
import com.zimanpro.pojo.registration.RegistrationResponse;
import com.zimanpro.pojo.send_otp.SendOTPResponse;
import com.zimanpro.pojo.verify_otp.VerifyOTPResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class LogInActivity extends AppCompatActivity {

    Button buttonNext;
    Button buttonProceed;
    Button buttonSubmit;
    TextView textViewResendOTP;
    TextView textViewResendOTPTimer;
    EditText editTextUserMobileNo;
    //    EditText editTextUserSAPIDNew;
    SessionManager sessionManager;
    AlertDialog b;
    RelativeLayout rl_mobile_number;
    RelativeLayout rl_otp;
    String otpRefNo;
    String userId;
    PinView pinViewOTP;
    String userMobileNo, token;
    String currentVersion = "", onlineVersion = "";
    int currentVersionCode, onlineVersionCode;
    //    RelativeLayout rl_sap_id;
    private AppWaitDialog mWaitDialog = null;
    AppUpdateManager appUpdateManager;
    //FakeAppUpdateManager appUpdateManager;
    LinearLayout linearLayout;
    CoordinatorLayout coordinatorLayout;
    //InstallStateUpdatedListener installStateUpdatedListener;
    private static final int RC_APP_UPDATE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token = " + token);
        if (!TextUtils.isEmpty(token)) {
            sessionManager.saveFCMToken(token);
        }

        // Creates instance of the manager.
        /*appUpdateManager = AppUpdateManagerFactory.create(LogInActivity.this);

        appUpdateManager.registerListener(installStateUpdatedListener);
        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE){
                    Toast.makeText(LogInActivity.this, "flexible Update available", Toast.LENGTH_LONG).show();
                }
                else if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        // For a flexible update, use AppUpdateType.FLEXIBLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                    // Request the update.
                    Toast.makeText(LogInActivity.this, "flexible Update available", Toast.LENGTH_LONG).show();

                    /*try {
                        appUpdateManager.startUpdateFlowForResult(
                                appUpdateInfo, AppUpdateType.FLEXIBLE, LogInActivity.this, RC_APP_UPDATE);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                }else if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        // For a flexible update, use AppUpdateType.FLEXIBLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)){
                    Toast.makeText(LogInActivity.this, "immediate Update available", Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(LogInActivity.this, "Update not available", Toast.LENGTH_LONG).show();
                }
            }
        });*/


        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

        linearLayout = findViewById(R.id.layoutl);
        //coordinatorLayout = findViewById(R.id.linearlay1);
        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        // Store values.
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("email", "purvak.p@dexoit.com");
        editor.putString("password", "123456");
        editor.apply();

        editTextUserMobileNo = findViewById(R.id.editTextUserMobileNo);
//        editTextUserSAPIDNew = findViewById(R.id.editTextUserSAPIDNew);

        textViewResendOTPTimer = findViewById(R.id.textViewResendOTPTimer);
        textViewResendOTP = findViewById(R.id.textViewResendOTP);
        textViewResendOTP.setEnabled(false);
        textViewResendOTP.setTextColor(getResources().getColor(R.color.light_grey));
        textViewResendOTP.setClickable(false);
        textViewResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP(userMobileNo, "2");
            }
        });

        rl_mobile_number = findViewById(R.id.rl_mobile_number);
        rl_otp = findViewById(R.id.rl_otp);
//        rl_sap_id = findViewById(R.id.rl_sap_id);

        buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLogInData();
            }
        });
        pinViewOTP = findViewById(R.id.pinViewOTP);

        buttonProceed = findViewById(R.id.buttonProceed);
        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = pinViewOTP.getText().toString();

                if (otp.isEmpty()) {
                    pinViewOTP.setError(getResources().getString(R.string.error_please_enter_verification));
                    pinViewOTP.requestFocus();
                    return;
                }

                if (otp.length() != 4) {
                    pinViewOTP.setError(getResources().getString(R.string.error_invalid));
                    pinViewOTP.requestFocus();
                    return;
                }


                verifyOTP(otp);
            }
        });

//        buttonSubmit = findViewById(R.id.buttonSubmit);
//        buttonSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
    }

    /*InstallStateUpdatedListener installStateUpdatedListener = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED){
                        popupSnackbarForCompleteUpdate();
                    } else if (state.installStatus() == InstallStatus.INSTALLED){
                        if (appUpdateManager != null){
                            appUpdateManager.unregisterListener(installStateUpdatedListener);
                        }

                    } else {
                        Log.i("TAG", "InstallStateUpdatedListener: state: " + state.installStatus());
                    }
                }
     };*/

    public void validateLogInData() {
//        String userSAPIDNew = editTextUserSAPIDNew.getText().toString();
        userMobileNo = editTextUserMobileNo.getText().toString();
        System.out.println("userMobileNo length = " + userMobileNo.length());
        if (InternetConnection.checkConnection(this)) {
            if (TextUtils.isEmpty(userMobileNo)) {
                editTextUserMobileNo.setError("Please enter mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userMobileNo) && userMobileNo.length() != 10) {
                editTextUserMobileNo.setError("Please enter valid mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

//            if (TextUtils.isEmpty(userSAPIDNew)) {
//                editTextUserSAPIDNew.setError("Please enter SAP Id");
//                editTextUserSAPIDNew.requestFocus();
//                return;
//            }
//
//            if (!TextUtils.isEmpty(userSAPIDNew) && userSAPIDNew.length() != 8) {
//                editTextUserSAPIDNew.setError("Please enter valid SAP ID");
//                editTextUserSAPIDNew.requestFocus();
//                return;
//            }

//            if (!TextUtils.isEmpty(userMobileNo) && !TextUtils.isEmpty(userSAPIDNew)) {
            if (!TextUtils.isEmpty(userMobileNo)) {
//                checkLoginSuccess(userMobileNo, userSAPIDNew);
//                checkLoginSuccess(userMobileNo);
                sendOTP(userMobileNo, "1");
            }
        } else {
            internetNotAvailableDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Log.e("TAG", "onActivityResult: app download failed");
            }
        }
    }

    /*private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        linearLayout,
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appUpdateManager != null) {
                    appUpdateManager.completeUpdate();
                }
            }
        });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }*/

    //    private void checkLoginSuccess(final String userMobileNo, final String userSAPIDNew) {
    private void checkLoginSuccess(final String userMobileNo) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("SendOTP URl = " + AppDataUrls.postSentOTP());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postLogin(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("LogIn Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        LogInResponse logInResponse = new Gson().fromJson(response, LogInResponse.class);
                        if (logInResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanpro.pojo.login.Result result = logInResponse.result;
                            if (result != null) {
//                                sessionManager.createLoginSession(true);
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName.concat(" ").concat(result.lastName));
//                                sessionManager.savePassword(result.password);
                                sessionManager.saveSAPCode(result.sapCode);
//                                sessionManager.saveFirstName(result.firstName);
//                                sessionManager.saveMiddleName(result.middleName);
//                                sessionManager.saveLastName(result.lastName);
//                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
//                                sessionManager.saveAge(result.age);
//                                sessionManager.saveGender(result.gender);
//                                sessionManager.saveBranchName(result.branchName);
                                if (result.locationInfo != null) {
                                    if (!TextUtils.isEmpty(result.locationInfo.branchName)) {
                                        sessionManager.saveBranchName(result.locationInfo.branchName);
                                    }
                                }
//                                sessionManager.saveDepartment(result.department);
//                                sessionManager.saveDesignation(result.designation);
//                                sessionManager.saveDeviceToken(result.deviceToken);
//                                sessionManager.saveActivePlatform(result.activePlatform);
                                sessionManager.saveAuthKey(result.authKey);
//                                sessionManager.saveCreatedAt(result.createdAt);
//                                sessionManager.saveModifiedAt(result.modifiedAt);
//                                sessionManager.saveIsDeleted(result.isDeleted);
                                sendOTP(userMobileNo, "1");
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, logInResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
//                                    checkLoginSuccess(userMobileNo, userSAPIDNew);
                                    checkLoginSuccess(userMobileNo);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
//                params.put(AppConstants.SAP_ID, userSAPIDNew);
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());

                System.out.println("LogIn Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void sendOTP(final String userMobileNo, final String purpose) {

        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSentOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("SendOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        SendOTPResponse sendOTPResponse = new Gson().fromJson(response, SendOTPResponse.class);
                        if (sendOTPResponse.status.equals(AppConstants.SUCCESS)) {
                            sessionManager.saveMobileNo(userMobileNo);
                            rl_mobile_number.setVisibility(View.GONE);
                            rl_otp.setVisibility(View.VISIBLE);
                            // rl_sap_id.setVisibility(View.GONE);
                            new CountDownTimer(30000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    long l = millisUntilFinished / 1000;
                                    if (l > 9) {
                                        textViewResendOTPTimer.setText("(00:" + l + ")");
                                    } else {
                                        textViewResendOTPTimer.setText("(00:0" + l + ")");
                                    }
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    textViewResendOTPTimer.setText("");
                                    textViewResendOTP.setTextColor(getResources().getColor(R.color.grey));
                                    textViewResendOTP.setEnabled(true);
                                    textViewResendOTP.setClickable(true);
                                }

                            }.start();

                            com.zimanpro.pojo.send_otp.Result result = sendOTPResponse.result;
                            if (result != null) {
                                otpRefNo = result.otpRefNo;
                                userId = result.userId;

                                rl_mobile_number.setVisibility(View.GONE);
                                rl_otp.setVisibility(View.VISIBLE);
//                                rl_sap_id.setVisibility(View.VISIBLE);

//                                pinViewOTP.setText(result.otp + "");
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, sendOTPResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    sendOTP(userMobileNo, purpose);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
                params.put(AppConstants.PURPOSE, purpose);
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showpopup(String message) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog2, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewTitle1 = alertLayout.findViewById(R.id.textViewTitle1);
        textViewTitle.setText("Verification Unsuccessful.");
        textViewTitle1.setText("User is Inactive.");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        //TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                startLogInActivity();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void startLogInActivity() {
        Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void verifyOTP(final String otp) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVerifyOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VerifyOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VerifyOTPResponse verifyOTPResponse = new Gson().fromJson(response, VerifyOTPResponse.class);
                        if (verifyOTPResponse.status.equals(AppConstants.SUCCESS)) {
//                            Toast.makeText(LogInActivity.this, verifyOTPResponse.result, Toast.LENGTH_LONG).show();
//                            sessionManager.createLoginSession(true);

                            sessionManager.saveUserId(verifyOTPResponse.result.user_id);
                            sessionManager.saveAuthKey(verifyOTPResponse.result.auth_key);
                            if (!TextUtils.isEmpty(verifyOTPResponse.result.user_id) && verifyOTPResponse.result.user_id.equals("0")) {
                                Intent intent = new Intent(LogInActivity.this, RegistrationActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                UpdateToken(userMobileNo, token);
                                getUserDetails(verifyOTPResponse.result.user_id, verifyOTPResponse.result.auth_key);
                            }

                        } else {
                            showpopup(verifyOTPResponse.message);
                            //Toast.makeText(LogInActivity.this, verifyOTPResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    verifyOTP(otp);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
//                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
//                params.put(AppConstants.USER_ID, userId);
                params.put(AppConstants.OTP_REF_NO, otpRefNo);
                params.put(AppConstants.OTP, otp);

                System.out.println("VerifyOTP Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void UpdateToken(final String mobileNo, final String token) {
        /*AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/

        System.out.println("updateToken URl = " + AppDataUrls.updateToken());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateToken(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getUserDetails Res = ", response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals(AppConstants.SUCCESS)){
                                //Toast.makeText(LogInActivity.this, "token Updated", Toast.LENGTH_LONG).show();
                            }else {
                                Toast.makeText(LogInActivity.this, "Error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    UpdateToken(mobileNo, token);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.DEVICE_TOKEN, token);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.MOBILE_NO, mobileNo);

                System.out.println("Sign up Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void getUserDetails(final String userId, final String authKey) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("getUserDetails URl = " + AppDataUrls.getUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getUserDetails Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanpro.pojo.registration.Result result = registrationResponse.result;
                            if (result != null) {
//                                sessionManager.createLoginSession(true);
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
//                                sessionManager.saveSAPCode(result.sapCode);
//                                sessionManager.saveFirstName(result.firstName);
//                                sessionManager.saveMiddleName(result.middleName);
//                                sessionManager.saveLastName(result.lastName);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);

//                                sessionManager.saveBranchName(result.branchName);
                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }
//                                sessionManager.saveDepartment(result.department);
//                                sessionManager.saveDesignation(result.designation);
                                sessionManager.saveDeviceToken(result.deviceToken);
//                                sessionManager.saveActivePlatform(result.activePlatform);
                                sessionManager.saveAuthKey(result.authKey);
//                                sessionManager.saveCreatedAt(result.createdAt);
//                                sessionManager.saveModifiedAt(result.modifiedAt);
//                                sessionManager.saveIsDeleted(result.isDeleted);

                                if (result.emergencyContacts.isEmpty()) {
                                    Intent intent = new Intent(LogInActivity.this, AddEmergencyContactActivity.class);
                                    intent.putExtra("From", "Login");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(LogInActivity.this, TnCnPolicyActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getUserDetails(userId, authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, authKey);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, userId);

                System.out.println("Sign up Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
//                finish();
            }
        });
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                                //onlineVersionCode = sibElemet.hashCode();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(16);
                    textViewTitle.setText(" Update info !");
                    TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
                    textViewMsg.setVisibility(View.VISIBLE);
                    textViewMsg.setTextSize(14);
                    textViewMsg.setText("New version is Available. Please update application");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    /*AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
                    alertDialog.setTitle("Update");
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setMessage("New Update is available");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();*/
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

}
