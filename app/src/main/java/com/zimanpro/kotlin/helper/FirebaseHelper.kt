package com.say.mahindrafinance.kotlin.helper

import android.util.Log
import com.google.firebase.database.*
import com.say.mahindrafinance.kotlin.model.Driver
import java.util.*

class FirebaseHelper constructor(driverId: String) {

    private val mTransportLocationStatuses = LinkedList<Map<String, Any>>()

    companion object {
        private const val ONLINE_DRIVERS = "online_drivers"
        private const val DATA_RECEIVER = "dataReceiver"
    }

    private val onlineDriverDatabaseReference: DatabaseReference = FirebaseDatabase
            .getInstance()
            .reference
            .child(ONLINE_DRIVERS)
            .child(driverId)
            .child("location")

    private val onlineDriverDataReceiverDatabaseReference: DatabaseReference = FirebaseDatabase
            .getInstance()
            .reference
            .child(ONLINE_DRIVERS)
            .child(driverId)
            .child(DATA_RECEIVER)

    init {
//        initMenu();

//        onlineDriverDatabaseReference
//                .onDisconnect()
//                .removeValue()
//
//        onlineDriverDataReceiverDatabaseReference
//                .onDisconnect()
//                .removeValue()
    }

    fun updateDataReceiver(datalist : List<String>) {
        onlineDriverDataReceiverDatabaseReference
                .setValue(datalist)
        Log.e("Device Token", " Updated")
    }

    fun updateDriver(driver: Driver) {

        val transportStatus = HashMap<String, Any>()
        transportStatus["lat"] = driver.lat
        transportStatus["lng"] = driver.lng
        transportStatus["time"] = driver.time
        transportStatus["power"] = driver.power

        mTransportLocationStatuses.addFirst(transportStatus);

        onlineDriverDatabaseReference
                .setValue(mTransportLocationStatuses)
//        onlineDriverDatabaseReference
//                .setValue(driver)
        Log.e("Driver Info", " Updated")
    }

    private fun initMenu() {
        val menuListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                print(dataSnapshot);
//                if (dataSnapshot !=null){
//                        for (transportStatus in dataSnapshot.getChildren()) {
//                            mTransportLocationStatuses.add(Integer.parseInt(transportStatus.getKey()!!),
//                                    transportStatus.getValue() as Map<String, Any>)
//                        }
//                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        }
        onlineDriverDatabaseReference.addListenerForSingleValueEvent(menuListener)
    }

    fun deleteDriver() {
//        onlineDriverDatabaseReference
//                .removeValue()
    }
}