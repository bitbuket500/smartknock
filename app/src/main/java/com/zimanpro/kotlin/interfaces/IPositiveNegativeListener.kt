package com.say.mahindrafinance.kotlin.interfaces

@FunctionalInterface
interface IPositiveNegativeListener {

    fun onPositive()

    fun onNegative() {

    }
}
