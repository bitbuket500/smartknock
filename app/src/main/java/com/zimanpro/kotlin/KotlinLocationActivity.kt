package com.say.mahindrafinance.kotlin

//class KotlinLocationActivity : AppCompatActivity() {
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_kotlin_location)
//    }
//}

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.*
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.say.mahindrafinance.R
import com.say.mahindrafinance.activities.GetTrackingActivity
import com.say.mahindrafinance.base.MahindraFinanceApplication
import com.say.mahindrafinance.helperClasses.*
import com.say.mahindrafinance.interfaces.AppDataUrls
import com.say.mahindrafinance.pojo.ContactInfo
import com.say.mahindrafinance.pojo.emergency_contacts.EmergencyContactsResponse
import com.say.mahindrafinance.pojo.emergency_contacts.Result
import com.say.mahindrafinance.pojo.set_tracking.SetTrackingResponse
import com.say.mahindrafinance.tracking.TrackerService
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class KotlinLocationActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    internal lateinit var mMapView: MapView
    internal lateinit var mLastLocation: Location
    internal var zoomSize = 10
    internal lateinit var mCurrLocationMarker: Marker
    internal lateinit var mGoogleApiClient: GoogleApiClient
    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var mContext: Context
    internal lateinit var sessionManager: SessionManager

    internal var i = 3
    internal lateinit var textViewTime: TextView
    internal lateinit var emergencyContactsAdapter: ContactAdapter
    internal var timeStrings = arrayOf<String>("For 30 Minutes", "For 1 Hour", "For 1 Hour 30 Minutes", "For 2 Hours", "For 2 Hours 30 Minutes", "For 3 Hours", "For 3 Hours 30 Minutes", "For 4 Hours", "For 4 Hours 30 Minutes", "For 5 Hours", "For 5 Hours 30 Minutes", "For 6 Hours", "For 6 Hours 30 Minutes", "For 7 Hours", "For 7 Hours 30 Minutes", "For 8 Hours")
    internal lateinit var contactInfo: ContactInfo
    private lateinit var cardViewGetStarted: CardView
    private lateinit var cardViewDetails: CardView
    private lateinit var cardViewSharingLocationWith: CardView
    private lateinit var imageViewClose: ImageView
    private lateinit var textViewSharingTime: TextView
    private lateinit var mMap: GoogleMap
    private lateinit var mWaitDialog: AppWaitDialog
    private lateinit var popWindowComment: PopupWindow
    private lateinit var mDetectorComment: GestureDetector
    private lateinit var recyclerEmergencyContacts: RecyclerView
    private var trackingStatus = "0"
    private var trackingId = 0
    private lateinit var mPrefs: SharedPreferences
    /**
     * Receives status messages from the tracking service.
     */
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // setTrackingStatus(intent.getIntExtra(getString(R.string.status), 0));
        }
    }
    internal lateinit var marker: Marker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_location)
        // add back arrow to toolbar
//        if (getSupportActionBar() != null)
//        {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true)
//            getSupportActionBar().setDisplayShowHomeEnabled(true)
//        }
        sessionManager = SessionManager(this)
        mWaitDialog = AppWaitDialog(this)
        mContext = this@KotlinLocationActivity
//        mMapView = findViewById(R.id.supportMap)
//        mMapView.onCreate(savedInstanceState)
//        checkLocationPermission(this)
//        mMapView.onResume() // needed to get the map to display immediately
//        try {
//            MapsInitializer.initialize(this)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        mMapView.getMapAsync(this)



        imageViewClose = findViewById(R.id.imageViewClose)
        imageViewClose.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                updateTrackingApiCall()
            }
        })
        textViewSharingTime = findViewById(R.id.textViewSharingTime)
        cardViewSharingLocationWith = findViewById(R.id.cardViewSharingLocationWith)
        cardViewDetails = findViewById(R.id.cardViewDetails)
        cardViewGetStarted = findViewById(R.id.cardViewGetStarted)
        cardViewGetStarted.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showBottomDialog(v)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_tracking, menu)
        // Find the menu item we are interested in.
        // MenuItem it = menu.findItem(R.id.menu_chat);
        return true
    }

    private fun updateTrackingApiCall() {
        AndroidUtils.hideKeyboard(this@KotlinLocationActivity)
        if (mWaitDialog != null) {
            mWaitDialog.show()
        }

        val params = HashMap<String, String>()
        params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW)
        params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
        params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
        params.put(AppConstants.TRACKING_ID, (trackingId).toString())
        if (trackingStatus == "1") {
            params.put(AppConstants.STATUS, "0")
        } else {
            params.put(AppConstants.STATUS, "1")
        }
        println("UpdateTracking = " + params.toString())

        val jsonObject = JSONObject(params)


        // Volley post request with parameters
        val request = object : JsonObjectRequest(Request.Method.POST, AppDataUrls.postSetTracking(), jsonObject,
                Response.Listener { response ->

                    Log.d("Update Tracking Res = ", response.toString())
                    if (mWaitDialog != null && mWaitDialog.isShowing) {
                        mWaitDialog.dismiss()
                    }
                    try {
                        val jsonObject = JSONObject(response.toString())
                        val status = jsonObject.getString("status")
                        if (status == AppConstants.SUCCESS) {
                            stopLocationService()
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications")
                            Toast.makeText(mContext, "Location sharing service stopped.", Toast.LENGTH_SHORT).show()
                            cardViewSharingLocationWith.visibility = GONE
                            cardViewDetails.visibility = View.VISIBLE
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener {
            if (mWaitDialog != null && mWaitDialog.isShowing) {
                mWaitDialog.dismiss()
            }
            if (!InternetConnection.checkConnection(this@KotlinLocationActivity)) {
                val dialogBuilder = AlertDialog.Builder(this@KotlinLocationActivity)
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                dialogBuilder.setView(dialogView)
                val b = dialogBuilder.create()
                b.show()
                dialogBuilder.setCancelable(false)
                val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                btnOkay.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        b.dismiss()
                    }
                })
            }
        }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }


       /* val stringRequest = object : StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(),
                object : Response.Listener<String> {
                    override fun onResponse(response: String) {
                        Log.d("Update Tracking Res = ", response)
                        if (mWaitDialog != null && mWaitDialog.isShowing) {
                            mWaitDialog.dismiss()
                        }
                        try {
                            val jsonObject = JSONObject(response)
                            val status = jsonObject.getString("status")
                            if (status == AppConstants.SUCCESS) {
                                stopLocationService()
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications")
                                Toast.makeText(mContext, "Location sharing service stopped.", Toast.LENGTH_SHORT).show()
                                cardViewSharingLocationWith.visibility = GONE
                                cardViewDetails.visibility = View.VISIBLE
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        if (mWaitDialog != null && mWaitDialog.isShowing) {
                            mWaitDialog.dismiss()
                        }
                        if (!InternetConnection.checkConnection(this@KotlinLocationActivity)) {
                            val dialogBuilder = AlertDialog.Builder(this@KotlinLocationActivity)
                            val inflater = layoutInflater
                            val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                            dialogBuilder.setView(dialogView)
                            val b = dialogBuilder.create()
                            b.show()
                            dialogBuilder.setCancelable(false)
                            val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                            btnOkay.setOnClickListener(object : View.OnClickListener {
                                override fun onClick(v: View) {
                                    b.dismiss()
                                }
                            })
                        }
                    }
                }) {
            //            protected val params: Map<String, String>
//                get() {
//                    val params = HashMap<String, String>()
//                    params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
//                    params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
//                    params.put(AppConstants.TRACKING_ID, (trackingId).toString())
//                    if (trackingStatus == "1") {
//                        params.put(AppConstants.STATUS, "0")
//                    } else {
//                        params.put(AppConstants.STATUS, "1")
//                    }
//                    println("UpdateTracking = " + params.toString())
//                    return params
//                }
//            val headers: Map<String, String>
//                @Throws(AuthFailureError::class)
//                get() {
//                    val params = HashMap<String, String>()
//                    params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW)
//                    return params
//                }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }*/

        request.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        MahindraFinanceApplication.getInstance().addToRequestQueue(request)
    }

    private fun showBottomDialog(view: View) {
        // inflate the layout of the popup window
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val inflatedViewComment = inflater.inflate(R.layout.pop_up_location_sharing_dialog, null)
        // inflatedViewComment.setPadding(0,100,0,0);
        recyclerEmergencyContacts = inflatedViewComment.findViewById(R.id.recyclerEmergencyContacts)
        recyclerEmergencyContacts.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        recyclerEmergencyContacts.itemAnimator = DefaultItemAnimator()
        val getEmergencyContacts = JsonCacheHelper.readFromJson(this@KotlinLocationActivity, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME)
        val emergencyContactsResponse = Gson().fromJson(getEmergencyContacts, EmergencyContactsResponse::class.java)
        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
            val emergencyContacts = emergencyContactsResponse.result
            emergencyContactsAdapter = ContactAdapter(this, emergencyContacts)
            recyclerEmergencyContacts.adapter = emergencyContactsAdapter
            emergencyContactsAdapter.notifyDataSetChanged()
        }
        textViewTime = inflatedViewComment.findViewById(R.id.textViewTime)
        textViewTime.text = timeStrings[3]
        val imageViewMinus = inflatedViewComment.findViewById(R.id.imageViewMinus) as ImageView
        imageViewMinus.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                i--
                if (i < 0) {
                    i = 0
                    textViewTime.text = timeStrings[0]
                } else {
                    textViewTime.text = timeStrings[i]
                }
            }
        })
        val imageViewPlus = inflatedViewComment.findViewById(R.id.imageViewPlus) as ImageView
        imageViewPlus.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                i++
                if (i >= timeStrings.size) {
                    i = timeStrings.size
                    textViewTime.text = timeStrings[i]
                } else {
                    textViewTime.text = timeStrings[i]
                }
            }
        })
        // cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare);
        cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare)
        cardViewShare.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val selected = emergencyContactsAdapter.selected
                val result = selected.get(0)
                // String s = textViewTime.getText().toString();
                var share_time = "120"
                when (i) {
                    0 -> share_time = "30" //For 30 Minutes
                    1 -> share_time = "60" // For 1 Hour
                    2 -> share_time = "90" // For 1 Hour 30 Minutes
                    3 -> share_time = "120" // For 2 Hours
                    4 -> share_time = "150" // For 2 Hours 30 Minutes
                    5 -> share_time = "180"// For 3 Hours
                    6 -> share_time = "210"// For 3 Hours 30 Minutes
                    7 -> share_time = "240"// For 4 Hours
                    8 -> share_time = "270"// For 4 Hours 30 Minutes
                    9 -> share_time = "300"// For 5 Hours
                    10 -> share_time = "330"// For 5 Hours 30 Minutes
                    11 -> share_time = "360"// For 6 Hours
                    12 -> share_time = "390"// For 6 Hours 30 Minutes
                    13 -> share_time = "420"// For 7 Hours
                    14 -> share_time = "450"// For 7 Hours 30 Minutes
                    15 -> share_time = "480"// For 8 Hours
                }
                val firebaseAuthKeyList = ArrayList<String>()
                for (i in selected.indices) {
                    val firebaseAuthKey = selected.get(i).fcmToken
                    firebaseAuthKeyList.add(firebaseAuthKey)
                }
                setTackingApiCall(result.userId, share_time, firebaseAuthKeyList)
                // Double latitude = mLastLocation.getLatitude();
                // Double longitude = mLastLocation.getLongitude();
                //
                // String uri = "http://maps.google.com/maps?saddr=" +latitude+","+longitude;
                //
                // Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                // sharingIntent.setType("text/plain");
                // String ShareSub = "Here is my location";
                // sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
                // sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
                // startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        })
        // get the gesture detector
        mDetectorComment = GestureDetector(inflatedViewComment.context, MyGestureListenerComment())
        // Add a touch listener to the view
        // The touch listener passes all its events on to the gesture detector
        inflatedViewComment.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                return mDetectorComment.onTouchEvent(event)
            }
        })
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it
        popWindowComment = PopupWindow(inflatedViewComment, width, height, focusable)
        popWindowComment.isOutsideTouchable = true
        popWindowComment.animationStyle = R.style.PopupAnimation
        popWindowComment.showAtLocation(view, Gravity.BOTTOM, 0, 0)
        dimBehind(popWindowComment)
    }

    private fun setTackingApiCall(tracker_id: String, share_time: String, firebaseAuthKeyList: ArrayList<String>) {
        AndroidUtils.hideKeyboard(this@KotlinLocationActivity)
        if (mWaitDialog != null) {
            mWaitDialog.show()
        }

        val params = HashMap<String, String>()
        params.put(AppConstants.TRACKEE_ID, sessionManager.userId)
        params.put(AppConstants.TRACKER_ID, tracker_id)
        params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
        params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
        params.put(AppConstants.SHARE_TIME, share_time)
        println("SetTracking Params =" + params.toString())

        val jsonObject = JSONObject(params)


        // Volley post request with parameters
        val request = object : JsonObjectRequest(Request.Method.POST, AppDataUrls.postSetTracking(), jsonObject,
                Response.Listener { response ->

                    Log.d("setTracking Response = ", response.toString())
                    if (mWaitDialog != null && mWaitDialog.isShowing) {
                        mWaitDialog.dismiss()
                    }
                    val setTrackingResponse = Gson().fromJson(response.toString(), SetTrackingResponse::class.java)
                    if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                        trackingId = setTrackingResponse.result.trackingId
                        trackingStatus = setTrackingResponse.result.status
                        popWindowComment.dismiss()
                        cardViewDetails.visibility = GONE
                        cardViewSharingLocationWith.visibility = View.VISIBLE
                        textViewSharingTime.text = (timeStrings[i])
                        checkAndStartLocationService(trackingId, firebaseAuthKeyList)
                    }
                }, Response.ErrorListener {
            if (mWaitDialog != null && mWaitDialog.isShowing) {
                mWaitDialog.dismiss()
            }
            if (!InternetConnection.checkConnection(this@KotlinLocationActivity)) {
                val dialogBuilder = AlertDialog.Builder(this@KotlinLocationActivity)
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                dialogBuilder.setView(dialogView)
                val b = dialogBuilder.create()
                b.show()
                dialogBuilder.setCancelable(false)
                val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                btnOkay.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        b.dismiss()
                    }
                })
            }
        }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        /*  val stringRequest = object : StringRequest(Request.Method.POST, AppDataUrls.postSetTracking(),jsonObject,
                  object : Response.Listener<String> {
                      override fun onResponse(response: String) {
                          Log.d("setTracking Response = ", response)
                          if (mWaitDialog != null && mWaitDialog.isShowing) {
                              mWaitDialog.dismiss()
                          }
                          val setTrackingResponse = Gson().fromJson(response, SetTrackingResponse::class.java)
                          if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                              trackingId = setTrackingResponse.result.trackingId
                              trackingStatus = setTrackingResponse.result.status
                              popWindowComment.dismiss()
                              cardViewDetails.visibility = GONE
                              cardViewSharingLocationWith.visibility = View.VISIBLE
                              textViewSharingTime.setText(timeStrings[i])
                              checkAndStartLocationService(trackingId, firebaseAuthKeyList)
                          }
                      }
                  },
                  object : Response.ErrorListener {
                      override fun onErrorResponse(error: VolleyError) {
                          if (mWaitDialog != null && mWaitDialog.isShowing) {
                              mWaitDialog.dismiss()
                          }
                          if (!InternetConnection.checkConnection(this@KotlinLocationActivity)) {
                              val dialogBuilder = AlertDialog.Builder(this@KotlinLocationActivity)
                              val inflater = layoutInflater
                              val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                              dialogBuilder.setView(dialogView)
                              val b = dialogBuilder.create()
                              b.show()
                              dialogBuilder.setCancelable(false)
                              val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                              btnOkay.setOnClickListener(object : View.OnClickListener {
                                  override fun onClick(v: View) {
                                      b.dismiss()
                                  }
                              })
                          }
                      }
                  }) {

          }*/

        request.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        MahindraFinanceApplication.getInstance().addToRequestQueue(request)
    }

    /**
     * First validation check - ensures that required inputs have been
     * entered, and if so, store them and runs the next check.
     */
    private fun checkAndStartLocationService(trackingId: Int, firebaseAuthKeyList: ArrayList<String>) {
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE)
        // Store values.
        val editor = mPrefs.edit()
        // editor.putString("transport_id", "Tracking_id_"+trackingId + "_" +sessionManager.getAuthKey());
        editor.putString("transport_id", "Tracking_id_" + trackingId)
        editor.putString("email", "purvak.p@dexoit.com")
        editor.putString("password", "123456")
        editor.putString("firebaseAuthKeyListString", Gson().toJson(firebaseAuthKeyList))
        editor.apply()
        // Validate permissions.
        startLocationService()
        // FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(TrackerService.STATUS_INTENT))
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onPause()
    }

    private fun startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        val pm = getSystemService(POWER_SERVICE) as PowerManager
        val intent = Intent()
        if (!pm.isIgnoringBatteryOptimizations(packageName)) {
            intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            intent.data = Uri.parse("package:" + packageName)
            startActivity(intent)
        }
        startService(Intent(this, TrackerService::class.java))
    }

    private fun stopLocationService() {
        stopService(Intent(this, TrackerService::class.java))
    }

    private fun emergencyContacts(): ArrayList<ContactInfo> {
        val list = ArrayList<ContactInfo>()
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Purvak Patel", "Purvak_patel@gmail.com", "9876543210")
        list.add(contactInfo)
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Jignesh Surti", "Jignesh@gmail.com", "8765432109")
        list.add(contactInfo)
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Kushal Gaitonde", "Kushal@gmail.com", "7654321098")
        list.add(contactInfo)
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Rahul Shah", "Rahul@gmail.com", "6543210987")
        list.add(contactInfo)
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Priyanka Chopra", "Priyanka@gmail.com", "5432109876")
        list.add(contactInfo)
        contactInfo = ContactInfo(R.drawable.ic_user_profile, "Rutuja Desai", "Rutuja@gmail.com", "1234567890")
        list.add(contactInfo)
        return list
    }

    fun checkLocationPermission(context: Context): Boolean {
        if ((ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) !== PackageManager.PERMISSION_GRANTED)) {
            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    AlertDialog.Builder(context)
                            .setTitle(R.string.title_location_permission)
                            .setMessage(R.string.text_location_permission)
                            .setPositiveButton(R.string.ok, object : DialogInterface.OnClickListener {
                                override fun onClick(dialogInterface: DialogInterface, i: Int) {
                                    //Prompt the user once explanation has been shown
                                    requestPermissions(
                                            arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                                            MY_PERMISSIONS_REQUEST_LOCATION)
                                }
                            })
                            .create()
                            .show()
                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                            MY_PERMISSIONS_REQUEST_LOCATION)
                }
            }
            return false
        } else {
            return true
        }
    }

    override fun onConnectionSuspended(i: Int) {
    }

    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        val latitude = location.latitude
        val longitude = location.longitude
        val latLng = LatLng(latitude, longitude)
        // mMap.addMarker(new MarkerOptions().position(latLng).title("My Location"));
        if (marker == null) {
            marker = mMap.addMarker(MarkerOptions().position(latLng))
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(mContext,
                            Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED)) {
                buildGoogleApiClient()
                mMap.isMyLocationEnabled = true
            }
        } else {
            buildGoogleApiClient()
            mMap.isMyLocationEnabled = true
        }
        mMap.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(latLng: LatLng) {
                // zoomSize = 10;
                // mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
            }
        })
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()
        mGoogleApiClient.connect()
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if ((ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED)) {
            if (mGoogleApiClient.isConnected) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                buildGoogleApiClient()
            }
        }
    }

    override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            finish() // close this activity and return to preview activity (if there is any)
        }
        if (item.itemId === R.id.menu_get_tracking) {
            //add the function to perform here
            val intent = Intent(this@KotlinLocationActivity, GetTrackingActivity::class.java)
            startActivity(intent)
            return (true)
        }
        return super.onOptionsItemSelected(item)
    }

    class ContactAdapter(ctx: Context, imageModelArrayList: ArrayList<Result>) : RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {
        override fun getItemCount(): Int {
            return all.size
        }

        private val inflater: LayoutInflater
        var all: ArrayList<Result>
        val selected: ArrayList<Result>
            get() {
                val selected = ArrayList<Result>()
                for (i in all.indices) {
                    if (all.get(i).isChecked) {
                        selected.add(all.get(i))
                    }
                }
                return selected
            }

        init {
            inflater = LayoutInflater.from(ctx)
            this.all = imageModelArrayList
        }

        fun setContacts(employees: ArrayList<Result>) {
            this.all = ArrayList<Result>()
            this.all = employees
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.MyViewHolder {
            val view = inflater.inflate(R.layout.contact_recycler_item, parent, false)
            val holder = MyViewHolder(view)
            return holder
        }

        override fun onBindViewHolder(holder: ContactAdapter.MyViewHolder, position: Int) {
            // holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
            holder.tvContactName.text = all.get(position).userName
            if (all.get(position).isChecked) {
                all.get(position).isChecked = (true)
                holder.ivContactSelected.visibility = View.VISIBLE
                // holder.ivContactSelected.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
            } else {
                holder.ivContactSelected.visibility = View.GONE
                all.get(position).isChecked = (false)
                // holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
            }




            holder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) : Unit {
                    if (!all.get(position).isChecked) {
                        all.get(position).isChecked = (true)
                        holder.ivContactSelected.visibility = View.VISIBLE
                    } else {
                        holder.ivContactSelected.visibility = View.GONE
                        all.get(position).isChecked = (false)
                    }
                    println("Selected Size = " + selected.size)
                    if (selected.size > 0) {
                        println("cardViewShare VISIBLE")
                        cardViewShare.visibility = View.VISIBLE
                    } else {
                        println("cardViewShare GONE")
                        cardViewShare.visibility = View.GONE
                    }
                }
            })
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tvContactName: TextView
            var ivContactImage: ImageView
            var ivContactSelected: ImageView

            init {
                tvContactName = itemView.findViewById(R.id.tvContactName)
                ivContactImage = itemView.findViewById(R.id.ivContactImage)
                ivContactSelected = itemView.findViewById(R.id.ivContactSelected)
            }
        }
    }

    private inner class MyGestureListenerComment : GestureDetector.OnGestureListener {
        override fun onDown(event: MotionEvent): Boolean {
            // Log.d("TAG","onDown: ");
            // don't return false here or else none of the other
            // gestures will work
            return true
        }

        override fun onShowPress(e: MotionEvent) {
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return false
        }

        override fun onLongPress(e: MotionEvent) {}
        override fun onScroll(e1: MotionEvent, e2: MotionEvent,
                              distanceX: Float, distanceY: Float): Boolean {
            // get device size
            val displayMetrics = resources.displayMetrics
            val width = displayMetrics.widthPixels
            val height = displayMetrics.heightPixels
            val h1 = height - e1.y as Int
            val h2 = height - e2.y as Int
            val halfHeight = height / 3
            if (height - h2 > (height / 2)) {
                popWindowComment.dismiss()
            }
            return true
        }

        override fun onFling(event1: MotionEvent, event2: MotionEvent,
                             velocityX: Float, velocityY: Float): Boolean {
            return false
        }
    }
    val MY_PERMISSIONS_REQUEST_LOCATION = 99
    companion object {

        private lateinit var cardViewShare: CardView

        fun dimBehind(popupWindow: PopupWindow) {
            val container: View
            if (popupWindow.background == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = popupWindow.contentView.parent as View
                } else {
                    container = popupWindow.contentView
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = popupWindow.contentView.parent.parent as View
                } else {
                    container = popupWindow.contentView.parent as View
                }
            }
            val context = popupWindow.contentView.context
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val p = container.layoutParams as WindowManager.LayoutParams
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            p.dimAmount = 0.6f
            wm.updateViewLayout(container, p)
        }
    }
}