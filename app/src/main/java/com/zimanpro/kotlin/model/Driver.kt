package com.say.mahindrafinance.kotlin.model

data class Driver(val lat: Double, val lng: Double, val time: Long, val power: Float)
//data class Driver(val lat: Double, val lng: Double, val time: Long, val power: Float,val driverId: String = "0000")