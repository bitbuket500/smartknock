package com.say.mahindrafinance.kotlin

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.say.mahindrafinance.R
import com.say.mahindrafinance.helperClasses.*
import com.say.mahindrafinance.interfaces.AppDataUrls
import com.say.mahindrafinance.kotlin.helper.FirebaseHelper
import com.say.mahindrafinance.kotlin.helper.GoogleMapHelper
import com.say.mahindrafinance.kotlin.helper.MarkerAnimationHelper
import com.say.mahindrafinance.kotlin.helper.UiHelper
import com.say.mahindrafinance.kotlin.interfaces.IPositiveNegativeListener
import com.say.mahindrafinance.kotlin.interfaces.LatLngInterpolator
import com.say.mahindrafinance.kotlin.model.Driver
import com.say.mahindrafinance.pojo.ContactInfo
import com.say.mahindrafinance.pojo.emergency_contacts.EmergencyContactsResponse
import com.say.mahindrafinance.pojo.emergency_contacts.Result
import com.say.mahindrafinance.pojo.set_tracking.SetTrackingResponse
import com.say.mahindrafinance.tracking.TrackerService
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class LocationTrackingActivity : AppCompatActivity() {

    private lateinit var cardViewGetStarted: CardView
    private lateinit var imageViewBackArrow: ImageView
    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var locationFlag = true
    private var driverOnlineFlag = false
    private var currentPositionMarker: Marker? = null
    private val googleMapHelper = GoogleMapHelper()
    private lateinit var firebaseHelper: FirebaseHelper;
    private val markerAnimationHelper = MarkerAnimationHelper()
    private val uiHelper = UiHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_tracking)

        sessionManager = SessionManager(this)
        mWaitDialog = AppWaitDialog(this)
        mContext = this@LocationTrackingActivity

        val mapFragment: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.supportMap) as SupportMapFragment
        mapFragment.getMapAsync(object : OnMapReadyCallback {
            override fun onMapReady(p0: GoogleMap?) {
                googleMap = p0!!
            }
        })
        createLocationCallback()
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = uiHelper.getLocationRequest()
        if (!uiHelper.isPlayServicesAvailable(this)) {
            Toast.makeText(this, "Play Services did not installed!", Toast.LENGTH_SHORT).show()
            finish()
        } else requestLocationUpdate()
        val driverStatusTextView = findViewById<TextView>(R.id.driverStatusTextView)
        findViewById<SwitchCompat>(R.id.driverStatusSwitch).setOnCheckedChangeListener { _, b ->
            driverOnlineFlag = b
            if (driverOnlineFlag) driverStatusTextView.text = resources.getString(R.string.online_driver)
            else {
                driverStatusTextView.text = resources.getString(R.string.offline)
                firebaseHelper.deleteDriver()
            }
        }


        imageViewClose = findViewById(R.id.imageViewClose)
        imageViewClose.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                driverOnlineFlag = false;

//                firebaseHelper.deleteDriver();

                updateTrackingApiCall()
            }
        })

        imageViewBackArrow = findViewById(R.id.imageViewBackArrow)
        imageViewBackArrow.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
               finish()
            }
        })


        textViewSharingTime = findViewById(R.id.textViewSharingTime)
        cardViewSharingLocationWith = findViewById(R.id.cardViewSharingLocationWith)
        cardViewDetails = findViewById(R.id.cardViewDetails)
        cardViewGetStarted = findViewById(R.id.cardViewGetStarted)
        cardViewGetStarted.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showBottomDialog(v)
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        if (!uiHelper.isHaveLocationPermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
            return
        }
        if (uiHelper.isLocationProviderEnabled(this))
            uiHelper.showPositiveDialogWithListener(this, resources.getString(R.string.need_location), resources.getString(R.string.location_content), object : IPositiveNegativeListener {
                override fun onPositive() {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            }, "Turn On", false)
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if (locationResult!!.lastLocation == null) return
                val latLng = LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                Log.e("Location", latLng.latitude.toString() + " , " + latLng.longitude)
                if (locationFlag) {
                    locationFlag = false
                    animateCamera(latLng)
                }
                if (driverOnlineFlag) firebaseHelper.updateDriver(Driver(lat = latLng.latitude, lng = latLng.longitude, time = Date().getTime(),power =  getBatteryLevel()))

                showOrAnimateMarker(latLng)
            }
        }
    }

    private fun getBatteryLevel(): Float {
        val batteryStatus = registerReceiver(null,
                IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        var batteryLevel = -1
        var batteryScale = 1
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel)
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale)
        }
        return batteryLevel / batteryScale.toFloat() * 100
    }

    private fun showOrAnimateMarker(latLng: LatLng) {
        if (currentPositionMarker == null)
            currentPositionMarker = googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(latLng))
        else markerAnimationHelper.animateMarkerToGB(currentPositionMarker!!, latLng, LatLngInterpolator.Spherical())
    }

    private fun animateCamera(latLng: LatLng) {
        val cameraUpdate = googleMapHelper.buildCameraUpdate(latLng)
        googleMap.animateCamera(cameraUpdate, 10, null)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            val value = grantResults[0]
            if (value == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show()
                finish()
            } else if (value == PackageManager.PERMISSION_GRANTED) requestLocationUpdate()
        }
    }

    internal var i = 3
    internal lateinit var textViewTime: TextView
    internal lateinit var emergencyContactsAdapter: ContactAdapter
    internal var timeStrings = arrayOf<String>("For 30 Minutes", "For 1 Hour", "For 1 Hour 30 Minutes", "For 2 Hours", "For 2 Hours 30 Minutes", "For 3 Hours", "For 3 Hours 30 Minutes", "For 4 Hours", "For 4 Hours 30 Minutes", "For 5 Hours", "For 5 Hours 30 Minutes", "For 6 Hours", "For 6 Hours 30 Minutes", "For 7 Hours", "For 7 Hours 30 Minutes", "For 8 Hours")
    internal lateinit var contactInfo: ContactInfo
    private lateinit var cardViewDetails: CardView
    private lateinit var cardViewSharingLocationWith: CardView
    private lateinit var imageViewClose: ImageView
    private lateinit var textViewSharingTime: TextView
    private lateinit var mMap: GoogleMap
    private lateinit var mWaitDialog: AppWaitDialog
    private lateinit var popWindowComment: PopupWindow
    private lateinit var mDetectorComment: GestureDetector
    private lateinit var recyclerEmergencyContacts: RecyclerView
    private var trackingStatus = "0"
    private var trackingId = 0
    private lateinit var mPrefs: SharedPreferences
    internal lateinit var mContext: Context
    internal lateinit var sessionManager: SessionManager

    private fun showBottomDialog(view: View) {
        // inflate the layout of the popup window
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val inflatedViewComment = inflater.inflate(R.layout.pop_up_location_sharing_dialog, null)
        // inflatedViewComment.setPadding(0,100,0,0);
        recyclerEmergencyContacts = inflatedViewComment.findViewById(R.id.recyclerEmergencyContacts)
        recyclerEmergencyContacts.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        recyclerEmergencyContacts.itemAnimator = DefaultItemAnimator()
        val getEmergencyContacts = JsonCacheHelper.readFromJson(this@LocationTrackingActivity, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME)
        val emergencyContactsResponse = Gson().fromJson(getEmergencyContacts, EmergencyContactsResponse::class.java)
        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
            val emergencyContacts = emergencyContactsResponse.result
            emergencyContactsAdapter = ContactAdapter(this, emergencyContacts)
            recyclerEmergencyContacts.adapter = emergencyContactsAdapter
            emergencyContactsAdapter.notifyDataSetChanged()
        }
        textViewTime = inflatedViewComment.findViewById(R.id.textViewTime)
        textViewTime.text = timeStrings[3]
        val imageViewMinus = inflatedViewComment.findViewById(R.id.imageViewMinus) as ImageView
        imageViewMinus.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                i--
                if (i < 0) {
                    i = 0
                    textViewTime.text = timeStrings[0]
                } else {
                    textViewTime.text = timeStrings[i]
                }
            }
        })
        val imageViewPlus = inflatedViewComment.findViewById(R.id.imageViewPlus) as ImageView
        imageViewPlus.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                i++
                if (i >= timeStrings.size) {
                    i = timeStrings.size
                    textViewTime.text = timeStrings[i]
                } else {
                    textViewTime.text = timeStrings[i]
                }
            }
        })
        // cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare);
        cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare)
        cardViewShare.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val selected = emergencyContactsAdapter.selected
                val result = selected.get(0)
                // String s = textViewTime.getText().toString();
                var share_time = "120"
                when (i) {
                    0 -> share_time = "30" //For 30 Minutes
                    1 -> share_time = "60" // For 1 Hour
                    2 -> share_time = "90" // For 1 Hour 30 Minutes
                    3 -> share_time = "120" // For 2 Hours
                    4 -> share_time = "150" // For 2 Hours 30 Minutes
                    5 -> share_time = "180"// For 3 Hours
                    6 -> share_time = "210"// For 3 Hours 30 Minutes
                    7 -> share_time = "240"// For 4 Hours
                    8 -> share_time = "270"// For 4 Hours 30 Minutes
                    9 -> share_time = "300"// For 5 Hours
                    10 -> share_time = "330"// For 5 Hours 30 Minutes
                    11 -> share_time = "360"// For 6 Hours
                    12 -> share_time = "390"// For 6 Hours 30 Minutes
                    13 -> share_time = "420"// For 7 Hours
                    14 -> share_time = "450"// For 7 Hours 30 Minutes
                    15 -> share_time = "480"// For 8 Hours
                }
                val firebaseAuthKeyList = ArrayList<String>()
                for (i in selected.indices) {
                    val firebaseAuthKey = selected.get(i).fcmToken
                    firebaseAuthKeyList.add(firebaseAuthKey)
                }
                setTackingApiCall(result.userId, share_time, firebaseAuthKeyList)
                // Double latitude = mLastLocation.getLatitude();
                // Double longitude = mLastLocation.getLongitude();
                //
                // String uri = "http://maps.google.com/maps?saddr=" +latitude+","+longitude;
                //
                // Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                // sharingIntent.setType("text/plain");
                // String ShareSub = "Here is my location";
                // sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
                // sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
                // startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        })
        // get the gesture detector
        mDetectorComment = GestureDetector(inflatedViewComment.context, MyGestureListenerComment())
        // Add a touch listener to the view
        // The touch listener passes all its events on to the gesture detector
        inflatedViewComment.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                return mDetectorComment.onTouchEvent(event)
            }
        })
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it
        popWindowComment = PopupWindow(inflatedViewComment, width, height, focusable)
        popWindowComment.isOutsideTouchable = true
        popWindowComment.animationStyle = R.style.PopupAnimation
        popWindowComment.showAtLocation(view, Gravity.BOTTOM, 0, 0)
        dimBehind(popWindowComment)
    }

    private inner class MyGestureListenerComment : GestureDetector.OnGestureListener {
        override fun onDown(event: MotionEvent): Boolean {
            // Log.d("TAG","onDown: ");
            // don't return false here or else none of the other
            // gestures will work
            return true
        }

        override fun onShowPress(e: MotionEvent) {
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return false
        }

        override fun onLongPress(e: MotionEvent) {}
        override fun onScroll(e1: MotionEvent, e2: MotionEvent,
                              distanceX: Float, distanceY: Float): Boolean {
            // get device size
            val displayMetrics = resources.displayMetrics
            val width = displayMetrics.widthPixels
            val height = displayMetrics.heightPixels
            val h1 = height - e1.y as Int
            val h2 = height - e2.y as Int
            val halfHeight = height / 3
            if (height - h2 > (height / 2)) {
                popWindowComment.dismiss()
            }
            return true
        }

        override fun onFling(event1: MotionEvent, event2: MotionEvent,
                             velocityX: Float, velocityY: Float): Boolean {
            return false
        }
    }

    class ContactAdapter(ctx: Context, imageModelArrayList: ArrayList<Result>) : RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {
        override fun getItemCount(): Int {
            return all.size
        }

        private val inflater: LayoutInflater
        var all: ArrayList<Result>
        val selected: ArrayList<Result>
            get() {
                val selected = ArrayList<Result>()
                for (i in all.indices) {
                    if (all.get(i).isChecked) {
                        selected.add(all.get(i))
                    }
                }
                return selected
            }

        init {
            inflater = LayoutInflater.from(ctx)
            this.all = imageModelArrayList
        }

        fun setContacts(employees: ArrayList<Result>) {
            this.all = ArrayList<Result>()
            this.all = employees
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.MyViewHolder {
            val view = inflater.inflate(R.layout.contact_recycler_item, parent, false)
            val holder = MyViewHolder(view)
            return holder
        }

        override fun onBindViewHolder(holder: ContactAdapter.MyViewHolder, position: Int) {
            // holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
            holder.tvContactName.text = all.get(position).userName
            if (all.get(position).isChecked) {
                all.get(position).isChecked = (true)
                holder.ivContactSelected.visibility = View.VISIBLE
                // holder.ivContactSelected.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
            } else {
                holder.ivContactSelected.visibility = View.GONE
                all.get(position).isChecked = (false)
                // holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
            }




            holder.itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) : Unit {
                    if (!all.get(position).isChecked) {
                        all.get(position).isChecked = (true)
                        holder.ivContactSelected.visibility = View.VISIBLE
                    } else {
                        holder.ivContactSelected.visibility = View.GONE
                        all.get(position).isChecked = (false)
                    }
                    println("Selected Size = " + selected.size)
                    if (selected.size > 0) {
                        println("cardViewShare VISIBLE")
                        cardViewShare.visibility = View.VISIBLE
                    } else {
                        println("cardViewShare GONE")
                        cardViewShare.visibility = View.GONE
                    }
                }
            })
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tvContactName: TextView
            var ivContactImage: ImageView
            var ivContactSelected: ImageView

            init {
                tvContactName = itemView.findViewById(R.id.tvContactName)
                ivContactImage = itemView.findViewById(R.id.ivContactImage)
                ivContactSelected = itemView.findViewById(R.id.ivContactSelected)
            }
        }
    }

    /**
     * Receives status messages from the tracking service.
     */
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // setTrackingStatus(intent.getIntExtra(getString(R.string.status), 0));
        }
    }

    companion object {
        private const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2200
        private lateinit var cardViewShare: CardView

        fun dimBehind(popupWindow: PopupWindow) {
            val container: View
            if (popupWindow.background == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = popupWindow.contentView.parent as View
                } else {
                    container = popupWindow.contentView
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = popupWindow.contentView.parent.parent as View
                } else {
                    container = popupWindow.contentView.parent as View
                }
            }
            val context = popupWindow.contentView.context
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val p = container.layoutParams as WindowManager.LayoutParams
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            p.dimAmount = 0.6f
            wm.updateViewLayout(container, p)
        }
    }

    private fun setTackingApiCall(tracker_id: String, share_time: String, firebaseAuthKeyList: ArrayList<String>) {
        AndroidUtils.hideKeyboard(this@LocationTrackingActivity)
        if (mWaitDialog != null) {
            mWaitDialog.show()
        }

        /*  val params = HashMap<String, String>()
  //        params[AppConstants.CONTENT_TYPE] = AppConstants.APPLICATION_WWW
          params[AppConstants.TRACKEE_ID] = sessionManager.userId
          params[AppConstants.TRACKER_ID] = tracker_id
          params[AppConstants.AUTH_KEY] = sessionManager.authKey
          params[AppConstants.APP_SECURITY_KEY] = AppConstants.APP_SECURITY_KEY_VALUE
          params[AppConstants.SHARE_TIME] = share_time
          println("SetTracking Params =" + params.toString())

          val jsonObject = JSONObject(params)


          // Volley post request with parameters
          val request = JsonObjectRequest(Request.Method.POST, AppDataUrls.postSetTracking(), jsonObject,
                  Response.Listener { response ->

                      Log.d("setTracking Response = ", response.toString())
                      if (mWaitDialog != null && mWaitDialog.isShowing) {
                          mWaitDialog.dismiss()
                      }
                      val setTrackingResponse = Gson().fromJson(response.toString(), SetTrackingResponse::class.java)
                      if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                          trackingId = setTrackingResponse.result.trackingId
                          trackingStatus = setTrackingResponse.result.status
                          popWindowComment.dismiss()
                          cardViewDetails.visibility = View.GONE
                          cardViewSharingLocationWith.visibility = View.VISIBLE
                          textViewSharingTime.text = (timeStrings[i])
                          checkAndStartLocationService(trackingId, firebaseAuthKeyList)
                      }
                  }, Response.ErrorListener {
              if (mWaitDialog != null && mWaitDialog.isShowing) {
                  mWaitDialog.dismiss()
              }
              if (!InternetConnection.checkConnection(this@LocationTrackingActivity)) {
                  val dialogBuilder = AlertDialog.Builder(this@LocationTrackingActivity)
                  val inflater = layoutInflater
                  val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                  dialogBuilder.setView(dialogView)
                  val b = dialogBuilder.create()
                  b.show()
                  dialogBuilder.setCancelable(false)
                  val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                  btnOkay.setOnClickListener(object : View.OnClickListener {
                      override fun onClick(v: View) {
                          b.dismiss()
                      }
                  })
              }
          })*//*{
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put(AppConstants.TRACKEE_ID, sessionManager.userId)
                params.put(AppConstants.TRACKER_ID, tracker_id)
                params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
                params.put(AppConstants.SHARE_TIME, share_time)
                return params
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }*/

          val stringRequest = object : StringRequest(Request.Method.POST, AppDataUrls.postSetTracking(),
                  Response.Listener<String> { response ->
                      Log.d("setTracking Response = ", response)
                      if (mWaitDialog != null && mWaitDialog.isShowing) {
                          mWaitDialog.dismiss()
                      }
                      val setTrackingResponse = Gson().fromJson(response, SetTrackingResponse::class.java)
                      if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                          trackingId = setTrackingResponse.result.trackingId
                          trackingStatus = setTrackingResponse.result.status
                          popWindowComment.dismiss()
                          cardViewDetails.visibility = View.GONE
                          cardViewSharingLocationWith.visibility = View.VISIBLE
                          textViewSharingTime.text = (timeStrings[i])

                          var driverId = "Tracking_id_".plus(trackingId);
                          firebaseHelper = FirebaseHelper(driverId)

                          val list = arrayListOf<String>()
                          list.add("1234567890");
                          list.add("2345678912");

                          firebaseHelper.updateDataReceiver(list)

                          driverOnlineFlag = true;

//                          checkAndStartLocationService(trackingId, firebaseAuthKeyList)
                      }
                  },
                  Response.ErrorListener {
                      if (mWaitDialog != null && mWaitDialog.isShowing) {
                          mWaitDialog.dismiss()
                      }
                      if (!InternetConnection.checkConnection(this@LocationTrackingActivity)) {
                          val dialogBuilder = AlertDialog.Builder(this@LocationTrackingActivity)
                          val inflater = layoutInflater
                          val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                          dialogBuilder.setView(dialogView)
                          val b = dialogBuilder.create()
                          b.show()
                          dialogBuilder.setCancelable(false)
                          val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                          btnOkay.setOnClickListener { b.dismiss() }
                      }
                  }) {
              override fun getParams(): Map<String, String> {
                  val params = HashMap<String, String>()
                  params[AppConstants.TRACKEE_ID] = sessionManager.userId
                  params[AppConstants.TRACKER_ID] = tracker_id
                  params[AppConstants.AUTH_KEY] = sessionManager.authKey
                  params[AppConstants.APP_SECURITY_KEY] = AppConstants.APP_SECURITY_KEY_VALUE
                  params[AppConstants.SHARE_TIME] = share_time

                  println("setTracking params = " + params)
                  return params
              }
              @Throws(AuthFailureError::class)
              override fun getHeaders(): Map<String, String> {
                  val headers = HashMap<String, String>()
                  headers["Content-Type"] = "application/x-www-form-urlencoded"
                  return headers
              }
          };

        stringRequest.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
//        ZimanProApplication.getInstance().addToRequestQueue(request)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }

    /**
     * First validation check - ensures that required inputs have been
     * entered, and if so, store them and runs the next check.
     */
    private fun checkAndStartLocationService(trackingId: Int, firebaseAuthKeyList: ArrayList<String>) {
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE)
        // Store values.
        val editor = mPrefs.edit()
        // editor.putString("transport_id", "Tracking_id_"+trackingId + "_" +sessionManager.getAuthKey());
        editor.putString("transport_id", "Tracking_id_" + trackingId)
        editor.putString("email", "purvak.p@dexoit.com")
        editor.putString("password", "123456")
        editor.putString("firebaseAuthKeyListString", Gson().toJson(firebaseAuthKeyList))
        editor.apply()
        // Validate permissions.
        startLocationService()
        // FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
    }

    private fun startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        val pm = getSystemService(POWER_SERVICE) as PowerManager
        val intent = Intent()
        if (!pm.isIgnoringBatteryOptimizations(packageName)) {
            intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            intent.data = Uri.parse("package:" + packageName)
            startActivity(intent)
        }
        startService(Intent(this, TrackerService::class.java))
    }

    private fun stopLocationService() {
        stopService(Intent(this, TrackerService::class.java))
    }

    private fun updateTrackingApiCall() {
        AndroidUtils.hideKeyboard(this@LocationTrackingActivity)
        if (mWaitDialog != null) {
            mWaitDialog.show()
        }

     /*   val params = HashMap<String, String>()
        params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW)
        params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
        params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
        params.put(AppConstants.TRACKING_ID, (trackingId).toString())
        if (trackingStatus == "1") {
            params.put(AppConstants.STATUS, "0")
        } else {
            params.put(AppConstants.STATUS, "1")
        }
        println("UpdateTracking = " + params.toString())

        val jsonObject = JSONObject(params)


        // Volley post request with parameters
        val request = object : JsonObjectRequest(Request.Method.POST, AppDataUrls.postSetTracking(), jsonObject,
                Response.Listener { response ->

                    Log.d("Update Tracking Res = ", response.toString())
                    if (mWaitDialog != null && mWaitDialog.isShowing) {
                        mWaitDialog.dismiss()
                    }
                    try {
                        val jsonObject = JSONObject(response.toString())
                        val status = jsonObject.getString("status")
                        if (status == AppConstants.SUCCESS) {
                            stopLocationService()
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications")
                            Toast.makeText(mContext, "Location sharing service stopped.", Toast.LENGTH_SHORT).show()
                            cardViewSharingLocationWith.visibility = View.GONE
                            cardViewDetails.visibility = View.VISIBLE
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }, Response.ErrorListener {
            if (mWaitDialog != null && mWaitDialog.isShowing) {
                mWaitDialog.dismiss()
            }
            if (!InternetConnection.checkConnection(this@LocationTrackingActivity)) {
                val dialogBuilder = AlertDialog.Builder(this@LocationTrackingActivity)
                val inflater = layoutInflater
                val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                dialogBuilder.setView(dialogView)
                val b = dialogBuilder.create()
                b.show()
                dialogBuilder.setCancelable(false)
                val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                btnOkay.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        b.dismiss()
                    }
                })
            }
        }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }*/


         val stringRequest = object : StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(),
                 object : Response.Listener<String> {
                     override fun onResponse(response: String) {
                         Log.d("Update Tracking Res = ", response)
                         if (mWaitDialog != null && mWaitDialog.isShowing) {
                             mWaitDialog.dismiss()
                         }
                         try {
                             val jsonObject = JSONObject(response)
                             val status = jsonObject.getString("status")
                             if (status == AppConstants.SUCCESS) {
                                 stopLocationService()
                                 FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications")
                                 Toast.makeText(mContext, "Location sharing service stopped.", Toast.LENGTH_SHORT).show()
                                 cardViewSharingLocationWith.visibility = View.GONE
                                 cardViewDetails.visibility = View.VISIBLE
                             }
                         } catch (e: JSONException) {
                             e.printStackTrace()
                         }
                     }
                 },
                 object : Response.ErrorListener {
                     override fun onErrorResponse(error: VolleyError) {
                         if (mWaitDialog != null && mWaitDialog.isShowing) {
                             mWaitDialog.dismiss()
                         }
                         if (!InternetConnection.checkConnection(this@LocationTrackingActivity)) {
                             val dialogBuilder = AlertDialog.Builder(this@LocationTrackingActivity)
                             val inflater = layoutInflater
                             val dialogView = inflater.inflate(R.layout.dialog_internet, null)
                             dialogBuilder.setView(dialogView)
                             val b = dialogBuilder.create()
                             b.show()
                             dialogBuilder.setCancelable(false)
                             val btnOkay = dialogView.findViewById(R.id.btnOkay) as Button
                             btnOkay.setOnClickListener(object : View.OnClickListener {
                                 override fun onClick(v: View) {
                                     b.dismiss()
                                 }
                             })
                         }
                     }
                 }) {
             override fun getParams(): Map<String, String> {
                 val params = HashMap<String, String>()
                 params[AppConstants.AUTH_KEY] = sessionManager.authKey
                 params[AppConstants.APP_SECURITY_KEY] = AppConstants.APP_SECURITY_KEY_VALUE
                 params[AppConstants.TRACKING_ID] = trackingId.toString()

                 if (trackingStatus == "1") {
                     params[AppConstants.STATUS] = "0"
                 } else {
                     params[AppConstants.STATUS] = "1"
                 }

                 println("UpdateTracking = $params")
                 return params
             }
             @Throws(AuthFailureError::class)
             override fun getHeaders(): Map<String, String> {
                 val headers = HashMap<String, String>()
                 headers["Content-Type"] = "application/x-www-form-urlencoded"
                 return headers
             }
         }



         /*{
             //            protected val params: Map<String, String>
 //                get() {
 //                    val params = HashMap<String, String>()
 //                    params.put(AppConstants.AUTH_KEY, sessionManager.authKey)
 //                    params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE)
 //                    params.put(AppConstants.TRACKING_ID, (trackingId).toString())
 //                    if (trackingStatus == "1") {
 //                        params.put(AppConstants.STATUS, "0")
 //                    } else {
 //                        params.put(AppConstants.STATUS, "1")
 //                    }
 //                    println("UpdateTracking = " + params.toString())
 //                    return params
 //                }
 //            val headers: Map<String, String>
 //                @Throws(AuthFailureError::class)
 //                get() {
 //                    val params = HashMap<String, String>()
 //                    params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW)
 //                    return params
 //                }
             @Throws(AuthFailureError::class)
             override fun getHeaders(): Map<String, String> {
                 val headers = HashMap<String, String>()
                 headers.put("Content-Type", "application/json")
                 return headers
             }
         }*/


        stringRequest.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
//        ZimanProApplication.getInstance().addToRequestQueue(request)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }

    class VolleySingleton constructor(context: Context) {
        companion object {
            @Volatile
            private var INSTANCE: VolleySingleton? = null
            fun getInstance(context: Context) =
                    INSTANCE ?: synchronized(this) {
                        INSTANCE ?: VolleySingleton(context).also {
                            INSTANCE = it
                        }
                    }
        }
        private val requestQueue: RequestQueue by lazy {
            // applicationContext is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            Volley.newRequestQueue(context.applicationContext)
        }
        fun <T> addToRequestQueue(req: Request<T>) {
            requestQueue.add(req)
        }
    }
}
